#pragma once

#include <XorString.h>

#include <MtFramework/Utils/cMtpvTexture.h>
#include <MtFramework/Utils/cMtpvFont.h>
#include <MtFramework/Common/typedefs.h>

#include "cMtpvCabal.h"

#include <d3dx9.h>
#include <functional>

using namespace std::placeholders;

///////////////////////////////////////////////////////////////////////////////
//  Definitions
///////////////////////////////////////////////////////////////////////////////

using tLootAnnouncerCallback = std::function<bool(UINT32 itemID, UINT32 itemOpt, UINT32 characterID, UINT32 channel, UINT32 map)>;
using tPAPDMathCallback = std::function<void(char* attack, char* defense)>;
using tPAPDCallback = std::function<void(UINT32 attack, UINT32 defense)>;

using tStdNotifyCreateDevice = std::function<void(Direct3D9Wrapper* pD3DWrapper, Direct3DDevice9Wrapper* pD3DDeviceWrapper)>;
using tStdD3DDevice = std::function<void(Direct3DDevice9Wrapper* _this)>;

using tEncryptStdCallback = std::function<void(pC2SHeader packet)>;
using tDecryptStdCallback = std::function<void(pS2CHeader packet)>;

using tOnLoadCallback = auto (*)() -> void;

// ///////////////////////////////////////////////////////////////////////////////
//  Cabal Class
///////////////////////////////////////////////////////////////////////////////

class cMtExportCabal
{
  public:
    virtual cMtpvCabal* GetClass() = 0;

    //Packet Callbacks
    virtual void OnEncryptCallback(tEncryptStdCallback callback) = 0;
    virtual void OnDecryptCallback(tDecryptStdCallback callback) = 0;

    //Loot Accouncer
    virtual void OnBeforeAnnouncementCallback(tLootAnnouncerCallback callback) = 0;

    //PA/PD
    virtual void OnPAPDCallback(tPAPDCallback callback) = 0;
    virtual void OnPAPDMathCallback(tPAPDMathCallback callback) = 0;
};


///////////////////////////////////////////////////////////////////////////////
//  DirectX Class
///////////////////////////////////////////////////////////////////////////////

class cMtExportDirectX
{
  public:
    virtual void OnNotifyCreateDeviceCallback(tStdNotifyCreateDevice callback) = 0;
    virtual void OnBeforePresentCallback(tStdD3DDevice callback) = 0;
    virtual void OnAfterPresentCallback(tStdD3DDevice callback) = 0;
    virtual void OnReleaseCallback(tStdD3DDevice callback) = 0;
};


///////////////////////////////////////////////////////////////////////////////
//  Utils Class
///////////////////////////////////////////////////////////////////////////////

class cMtExportUtils
{
public:
  virtual void DbgPrint(const char* format, ...) = 0;
  virtual void SendLog(char* msg) = 0;
  virtual bool StartConsole() = 0;
};


///////////////////////////////////////////////////////////////////////////////
//  Creator Class
///////////////////////////////////////////////////////////////////////////////

class cMtExportCreator
{
public:
  virtual cMtTexture* Texture() = 0;
  virtual cMtFont* Font() = 0;
};


///////////////////////////////////////////////////////////////////////////////
//  Core Class
///////////////////////////////////////////////////////////////////////////////

class cMtpvCore
{
  public:
    virtual void OnLoadCallback(tOnLoadCallback callback) = 0;

    virtual cMtExportDirectX* DirectX() = 0;
    virtual cMtExportCreator* Create() = 0;
    virtual cMtExportUtils* Utils() = 0;
    virtual cMtExportCabal* Cabal() = 0;
};

cMtpvCore* GetFramework(char* license);
