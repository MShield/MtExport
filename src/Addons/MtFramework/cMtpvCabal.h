#pragma once

#include <Windows.h>

typedef enum
{
  Warrior = 1,
  Blader,
  Wizzard,
  ForceArcher,
  ForceShielder,
  ForceBlader,
} Classes;

typedef enum
{
  NEUTRAL = 0,
  CAPELLA = 1,
  PROCYON = 2,
  GM = 3,
} Nation;

typedef enum
{
  CAttack                 = 0,
  CMagicAttack            = 2,
  CDefense                = 4,
  CAttackRate             = 5,
  CDefenseRate            = 6,
  CFleeRate               = 7,
  CCriticalRate           = 8,
  CCriticalDmg            = 10,
  CSwordSkillAmp          = 11,
  CMagicSkillAmp          = 12,
  CAddDamage              = 13,
  CMinDamage              = 14,
  CHPAbsorb               = 15, //not tested
  CMPAbsorb               = 16, //not tested
  CHPAbsorbLimitUp        = 17, //not tested
  CMPAbsorbLimitUp        = 18, //not tested
  CHPAutoHeal             = 19,
  CMPAutoHeal             = 20,
  CResistCriticalRate     = 21,
  CResistCriticalDamage   = 22,
  CResistStun             = 23, //not tested
  CResistDown             = 24, //not tested
  CResistKnockBack        = 25, //not tested
  CResistUnmovable        = 26, //not tested
  CResistSkillAmp         = 27,
  CMovementSpeed          = 28, //not tested
} CharacterMenuSection;

typedef enum
{
  Attack                  = 0,
  MagicAttack             = 1,
  Defense                 = 2,
  AttackRate              = 3,
  DefenseRate             = 4,
  FleeRate                = 5,
  CriticalRate            = 6,
  CriticalDmg             = 7,
  SwordSkillAmp           = 8,
  MagicSkillAmp           = 9,
  AddDamage               = 10,
  MinDamage               = 11,
  HPAbsorb                = 12,
  MPAbsorb                = 13,
  HPAbsorbLimitUp         = 14,
  MPAbsorbLimitUp         = 15,
  HPAutoHeal              = 16,
  MPAutoHeal              = 17,
  ResistCriticalRate      = 18,
  ResistCriticalDamage    = 19,
  ResistSkillAmp          = 20,
} CharacterData;

#pragma pack(push, 1)
typedef struct S2CHeader_Base
{
  UINT16	magic;
  UINT16	length;
  UINT16	opcode;
} S2CHeader_Base, * pS2CHeader_Base;

typedef struct C2SHeader_Base
{
  UINT16 magic;
  UINT16 length;
  UINT32 checksum;
  UINT16 opcode;
} C2SHeader_Base, * pC2SHeader_Base;

typedef struct C2SHeader : public C2SHeader_Base
{
  UINT8 data[1];
} C2SHeader, * pC2SHeader;

typedef struct S2CHeader : public S2CHeader_Base
{
  UINT8 data[1];
} S2CHeader, * pS2CHeader;

typedef struct Basic_Target_Data
{
  int index;
  BYTE targetType;
} Basic_Target_Data;

typedef int Buff_Result;

typedef struct Target_Data_Ex : public Basic_Target_Data
{
  Buff_Result result; //���� ���
} Target_Data_Ex;

typedef struct S2C_BASICBUFF : public S2CHeader_Base
{
  WORD skillIdx;
  WORD skillLv;
  BYTE buffType;
  BYTE targetCount;
  int skillExp;
  WORD currentMP;
  Target_Data_Ex targetData;
} S2C_BASICBUFF; //S2C_BASICBUFFF

typedef struct C2S_BASICBUFFF : public C2SHeader_Base
{
  BYTE buffType;
  BYTE targetCount;
  WORD skillIdx;
  /*...*/
} C2S_BASICBUFFF;

typedef struct
{
  char text[16];
  UINT length;
  UINT maxLength;
} CabalString, * pCabalString;

typedef struct
{
  UINT _dummy1[39];
  int warCooldownModifyByEachLevel;
  int warCooldownInMs;
  UINT skillID;
  UINT type;
  UINT _dummy2;
  int cooldownModifyByEachLevel;
  int cooldownInMs;
  UINT _dummy3[2];
  UINT tabIndex;
  UINT _dummy4[33];
  USHORT _dummy5;
  USHORT battleMode;        //-1 = enabled
  UINT rangeMax;            //Encrypted
  UINT rangeMin;            //Encrypted
  UINT area;                //Encrypted
} *pSkillInfo;

typedef struct
{
  UINT _dummy1;
  CabalString name;
  UINT _dummy2;
  CabalString iconName;
} *pSkillDesc;

typedef struct
{
  void* _dummy1;
  pSkillInfo skillInfo;
  pSkillDesc skillDesc;
  UINT _dummy2[4];
  int level;                   //Encrypted
  float cooldownTime;           //The time you casted the skill
  float cooldownUsedSkill;      //Set after it is called
  float cooldownUndefinedPart;
} *pSkill;

typedef struct
{
  UINT _dummy[0xBD4];
  int level;
  UINT _dummy1; //-1?
  UINT skillID;
  UINT _dummy2;
  pSkillInfo skillInfo;
} *pSkillCooldownData;

typedef struct S_ITEM_AXIS
{
	float xMale;
	float yMale;
	float zMale;
	float xFem;
	float yFem;
	float zFem;
} S_ITEM_AXIS;

typedef enum 
{
  k1X1 = 1,
  k1X2,
  k1X3,
  k1X4,
  k2X1,
  k2X2,
  k2X3,
  k2X4,
  k3X1,
  k3X2,
  k3X3,
  k3X4,
  k4X1,
  k4X2,
  k4X3,
  k4X4
} CabalItemSize;

typedef struct S_ITEM_INFO
{
	BYTE dummy1[8];
	DWORD removeHair;
	DWORD itemID;
	DWORD displayCode;
	DWORD displayCode2;
  S_ITEM_AXIS iTranslations;
  S_ITEM_AXIS iRotation;
	float maleScale;
	float femaleScale;
	DWORD levelLimit;
  Classes classes;
	UINT32 reputation;
	BYTE dummy2[31];
	UINT32 strLimit;
	UINT32 dexLimit;
	UINT32 intLimit;
	UINT32 sellPrice;
	CabalItemSize itemSize;
	DWORD leveltoUse;
	DWORD precisionOpt;
	DWORD evasionAndAttkOpt;
	DWORD defenseAndMAttkOpt;
	DWORD maxCore;
	UINT32 str_lmt_1__opt2;
	UINT32 dex_lmt_1__opt_2_val;
	UINT32 int_lmt_1__opt_3;
	UINT32 str_lmt_2__opt_3_val;
	UINT32 dex_lmt_2__opt_4;
	UINT32 int_lmt_2__opt_4_val;
	UINT32 d_str_1;
	UINT32 d_dex_1;
	UINT32 d_int_1;
	UINT32 property;
	BYTE enchantCodeLink;
	BYTE dummy3;
	BYTE grade;
	BYTE dummy4;
	Classes class1;
	Classes class2;
	BYTE typePeriod;
	BYTE usagePeriod;
	BYTE dummy5[5];
} S_ITEM_INFO;

typedef struct S_ITEM_PROPERTIES
{
	DWORD* unk1;  //0
	DWORD unk2;  //4
	DWORD unk3;  //8
	DWORD unk4;  //c
	DWORD unk5;  //10
	DWORD unk6;  //14
  S_ITEM_INFO* properties;  //18
	DWORD unk7;  //1c
	DWORD unk8;  //20
	DWORD unk9;  //24
	DWORD unk10;  //28
	DWORD unk11;  //2c
	DWORD unk12;  //30
	double unk13;  //34
	float unk14;  //3C
	DWORD upgradeLevel;  //40
	DWORD extended;  //44
	DWORD unk15;  //48
	DWORD unk16;  //4c
	DWORD unk17;  //50
	DWORD unk18;  //54
	DWORD unk19;  //58
	DWORD itemOpt;  //5c
	DWORD unk20;  //
	DWORD unk21;  //
} S_ITEM_PROPERTIES;

typedef struct S_ITEM
{
  S_ITEM_PROPERTIES* equipmentList[21];
  S_ITEM_PROPERTIES* inventoryList[256];
} S_ITEM;

typedef struct S_ITEM_PTR
{
  S_ITEM* item;
} S_ITEM_PTR;

typedef struct S_ITEM_DATA
{
  UINT32 itemID;
  UINT32 itemOpt;
} S_ITEM_DATA;
#pragma pack(pop)

class cMtpvBase
{
public:
    virtual int EncryptSkillValue(UINT value) = 0;
    virtual int DecryptSkillValue(UINT value) = 0;
    virtual UINT_PTR* GetBaseAddressPtr() = 0;
    virtual UINT DecryptValue(UINT value) = 0;
    virtual void Write2Screen(char* text, int val1 = 7, int val2 = 1) = 0;
    virtual bool IsCharLogged() = 0;
    virtual bool SetUsedSkillCooldown(UINT id, float seconds) = 0;
    virtual bool IsReady() = 0;
    virtual bool SetSkillCooldown(UINT id, float seconds) = 0;
    virtual pSkill FindSkill(UINT id, bool full = false) = 0;
    virtual bool SetSkillCooldown(UINT id, UINT seconds) = 0;
    virtual float MathCooldown(pSkill skill, int level) = 0;
    virtual int AdjustUI(int value, bool setUP = true) = 0;
    virtual bool IsBMWithAura(UINT state = (UINT)-1) = 0;
    virtual char* GetCabalString(pCabalString pCS) = 0;
    virtual float GetCurrentSkillCoolTime(UINT id) = 0;
    virtual bool IsAura(UINT state = (UINT)-1) = 0;
    virtual bool IsBM1(UINT state = (UINT)-1) = 0;
    virtual bool IsBM2(UINT state = (UINT)-1) = 0;
    virtual bool IsBM3(UINT state = (UINT)-1) = 0;
    virtual int GetHoveredCurrentSkillLevel() = 0;
    virtual int GetHoveredFutureSkillLevel() = 0;
    virtual float GetSkillCooldown(UINT id) = 0;
    virtual UINT_PTR GetExtrGameBaseAddress() = 0;
    virtual void SetCharLogged(UINT state) = 0;
    virtual pSkill GetSkill(UINT index) = 0;
    virtual void Send2Loud(char* text) = 0;
    virtual UINT GetDamageAmplifier() = 0;
    virtual char* GetCharacterName() = 0;
    virtual UINT GetPVPCharacterId() = 0;
    virtual UINT GetMyCharacterId() = 0;
    virtual UINT GetCharacterMode() = 0;
    virtual void SetTarget(UINT id) = 0;
    virtual float GetInGameTimer() = 0;
    virtual void SetGM(UINT value) = 0;
    virtual char* GetAccountName() = 0;
    virtual UINT GetCabalSocket() = 0;
    virtual UINT GetServerIndex() = 0;
    virtual UINT GetComboCount() = 0;
    virtual float GetWalkSpeed() = 0;
    virtual float GetNoCasting() = 0;
    virtual UINT GetIdByClick() = 0;
    virtual int GetUIPosition() = 0;
    virtual float GetNoDelay() = 0;
    virtual UINT GetViewType() = 0;
    virtual bool IsUIVisible() = 0;
    virtual bool IsInDungeon() = 0;
    virtual float GetUIScale() = 0;
    virtual UINT GetBowRange() = 0;
    virtual UINT GetWallAddr() = 0;
    virtual UINT GetVersion() = 0;
    virtual void RemoveAura() = 0;
    virtual UINT GetPKLevel() = 0;
    virtual UINT GetChannel() = 0;
    virtual UINT GetMapPost() = 0;
    virtual bool IsOnAstral() = 0;
    virtual UINT GetViewHP() = 0;
    virtual void RemoveBM1() = 0;
    virtual void RemoveBM2() = 0;
    virtual void RemoveBM3() = 0;
    virtual UINT GetNation() = 0;
    virtual UINT GetMapPre() = 0;
    virtual float GetTime() = 0;
    virtual UINT GetClass() = 0;
    virtual bool IsShowed() = 0;
    virtual UINT GetLevel() = 0;
    virtual UINT GetRange() = 0;
    virtual bool IsAlive() = 0;
    virtual UINT GetRank() = 0;
    virtual bool IsCombo() = 0;
    virtual UINT GetMap() = 0;
    virtual UINT GetAOE() = 0;
    virtual UINT GetGM() = 0;
    virtual bool IsPVP() = 0;
    virtual bool IsTG() = 0;
    
    virtual int GetCharacterMenuValue(CharacterMenuSection section) = 0;
    virtual int GetMyHP() = 0;
    virtual int GetCharacterData(CharacterData section) = 0;
    virtual int GetMyBaseHP() = 0;

    virtual void EncryptPacket(UINT_PTR encryptClass, UINT_PTR setupClass, pC2SHeader packet, UINT32 size) = 0;
    virtual void DecryptPacket(UINT_PTR _class, pS2CHeader packet, UINT32 size) = 0;
    virtual S_ITEM_PROPERTIES* GetInventoryItem(int index) = 0;
    virtual S_ITEM_DATA GetInventorySlotData(int index) = 0;
    virtual void SendPacket(void* p, UINT32 size) = 0;
    virtual char* GetGuildName() = 0;
    virtual void SetLastErrorCode(int error, int type) = 0;
    virtual void ShowCashItemArrival() = 0;
    virtual int GetStyle() = 0;
    virtual int GetSkillCooldown(pSkill skill, int level = 0) = 0;
};

typedef cMtpvBase cMtpvCabal;