#include "cMtpvCore.h"

cMtpvCore* GetFramework(char* license)
{
  using tGetCabalFramework = auto (*)(char*)->cMtpvCore*;

  cMtpvCore* pFunc = 0;
  
  if (auto _lib = GetModuleHandleA(XorString("mtd.dll")))
    pFunc = (cMtpvCore*)GetProcAddress(_lib, XorString("GetFramework"));

  return (pFunc ? ((tGetCabalFramework)pFunc)(license) : 0);
}