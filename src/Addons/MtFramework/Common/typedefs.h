#pragma once

#include <MtFramework/DirectX/Direct3D9Wrapper.h>

using tNotifyCreateDevice   = auto (*)(Direct3D9Wrapper* pD3DWrapper, Direct3DDevice9Wrapper* pD3DDeviceWrapper) -> void;
using tPresent              = auto (*)(Direct3DDevice9Wrapper* _this) -> void;
using tRelease              = tPresent;