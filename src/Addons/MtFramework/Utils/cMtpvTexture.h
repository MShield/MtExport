#pragma once

#include <d3dx9.h>

class cMtTexture;

using tWMHandler  = auto (*)(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, cMtTexture* _this, bool* forceRet) -> LRESULT;
using tCheckFlag  = auto (*)() -> bool;

#define WMHandler(func) LRESULT func(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, cMtTexture* _this, bool* forceRet)

class cMtTexture
{
  public:
    virtual cMtTexture* Create(IDirect3DDevice9* pDevice, const unsigned char* data, UINT size) = 0;
    virtual cMtTexture* Create(IDirect3DDevice9* pDevice, wchar_t* dir, char* fileName) = 0;
    /* Object's position */
    virtual cMtTexture* SetRect(int left, int top, int right = -1, int bottom = -1) = 0;
    /* Rezise the texture (from SetSize) */
    virtual cMtTexture* ScaleFromSizeIfHigher(UINT width, UINT height) = 0;
    virtual D3DXMATRIX* Transform(D3DXVECTOR2 pos, D3DXMATRIX* matrix) = 0;
    virtual cMtTexture* AddCallback(UINT uMsg, tWMHandler callback) = 0;
    virtual cMtTexture* ScaleFromSize(UINT width, UINT height) = 0;
    virtual cMtTexture* ScaleFromHeightIfHigher(UINT height) = 0;
    virtual cMtTexture* ScaleFromWidthIfHigher(UINT width) = 0;
    virtual cMtTexture* SetRect(POINT poses[1], int count) = 0;
    virtual cMtTexture* AC(UINT uMsg, tWMHandler callback) = 0;
    virtual cMtTexture* SetCheckFlag(tCheckFlag handler) = 0;
    /* Texture's object size */
    virtual cMtTexture* SetSize(UINT width, UINT height) = 0;
    virtual cMtTexture* ScaleFromHeight(UINT height) = 0;
    virtual BOOL IsInScreenRect(POINT pos, POINT pt) = 0;
    virtual UINT GetHeight(bool constSized = false) = 0;
    virtual UINT GetWidth(bool constSized = false) = 0;
    virtual cMtTexture* SetScale(D3DXVECTOR2 scale) = 0;
    virtual cMtTexture* Copy(cMtTexture* mtTexture) = 0;
    virtual cMtTexture* SetupWMHandler(HWND window) = 0;
    virtual cMtTexture* ScaleFromWidth(UINT width) = 0;
    virtual BOOL IsInRect(POINT pos, POINT pt) = 0;
    virtual cMtTexture* SetHeight(UINT height) = 0;
    virtual cMtTexture* SetWidth(UINT width) = 0;
    virtual cMtTexture* SetPos(int x, int y) = 0;
    virtual LPDIRECT3DTEXTURE9 GetTexture() = 0;
    virtual RECT GetClientRect(POINT pos) = 0;
    virtual void SetRectIndex(int index) = 0;
    virtual RECT* GetRect(int index) = 0;
    virtual cMtTexture* SetX(int x) = 0;
    virtual cMtTexture* SetY(int y) = 0;
    virtual RECT* GetIndexedRect() = 0;
    virtual cMtTexture* Release() = 0;
    virtual cMtTexture* Load() = 0;
    virtual int GetRectIndex() = 0;
    virtual UINT GetMemSize() = 0;
};
