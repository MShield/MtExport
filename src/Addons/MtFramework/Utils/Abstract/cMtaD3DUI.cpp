#include "cMtaD3DUI.h"

cMtaD3DUI::cMtaD3DUI()
{
  textures.clear();
  sprite = 0;
}

void cMtaD3DUI::DrawTexture(D3DXVECTOR2 pos, cMtTexture* pTexture, UINT8 alpha)
{
  D3DXMATRIX matrix;

  pTexture->Load();
  sprite->SetTransform(pTexture->Transform(pos, &matrix));
  sprite->Draw(pTexture->GetTexture(), pTexture->GetIndexedRect(), 0, 0, D3DCOLOR_RGBA(255, 255, 255, alpha));
}

void cMtaD3DUI::ReleaseTextures()
{
  if (!textures.empty())
  {
    for (auto it : textures)
      it->Release();
  }

  if (font)
    font->Release();

  if (sprite)
  {
    sprite->Release();
    sprite = 0;
  }
}

void cMtaD3DUI::Begin(Direct3DDevice9Wrapper* pDevice, DWORD Flags)
{
  if (!sprite)
    D3DXCreateSprite(pDevice, &sprite);

  sprite->Begin(Flags);
}

void cMtaD3DUI::End()
{
  sprite->End();
}

POINT cMtaD3DUI::AdjustRect(HWND hWnd, POINT pt, LONG right, LONG bottom)
{
  POINT ptNew;
  RECT rect;

  GetClientRect(hWnd, &rect);

  rect.left = 0;
  rect.top = 0;
  rect.right -= right;
  rect.bottom -= bottom;

  ptNew.x = (pt.x - xMPos);
  ptNew.y = (pt.y - yMPos);

  if (ptNew.x < rect.left)
    ptNew.x = rect.left;
  else if (ptNew.x > rect.right)
    ptNew.x = rect.right;

  if (ptNew.y < rect.top)
    ptNew.y = rect.top;
  else if (ptNew.y > rect.bottom)
    ptNew.y = rect.bottom;

  return ptNew;
}