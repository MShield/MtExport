#pragma once
#pragma warning(disable: 26495)

#include <MtFramework/DirectX/Direct3D9Wrapper.h>
#include <MtFramework/Utils/cMtpvTexture.h>
#include <MtFramework/Utils/cMtpvFont.h>

#include <Windows.h>

#include <list>

typedef struct { float x, y, z, rhw; UINT color; float px, py; } TVERTEX;

class cMtaD3DUI
{
  protected:
    D3DDEVICE_CREATION_PARAMETERS parameters;
    std::list<cMtTexture*> textures;
    LPD3DXSPRITE sprite;
    cMtFont* font;
    int xMPos;
    int yMPos;
    int xPos;
    int yPos;

    void DrawTexture(D3DXVECTOR2 pos, cMtTexture* pTexture, UINT8 alpha = -1);
    POINT AdjustRect(HWND hWnd, POINT pt, LONG right, LONG bottom);
    void Begin(Direct3DDevice9Wrapper* pDevice, DWORD flags);
    void ReleaseTextures();
    void End();

    cMtaD3DUI();
};