#pragma once

#include <d3dx9.h>

class cMtFont
{
  public:
    virtual cMtFont* Create(IDirect3DDevice9* pDevice, UINT height, UINT weight, DWORD quality, char* name) = 0;
    virtual cMtFont* SetRect(int left, int top, int right = -1, int bottom = -1) = 0;
    virtual cMtFont* SetTextAttr(D3DCOLOR color, DWORD format) = 0;
    virtual cMtFont* SetColor(D3DCOLOR color) = 0;
    virtual cMtFont* SetFormat(DWORD format) = 0;
    virtual cMtFont* SetQuality(UINT height) = 0;
    virtual cMtFont* SetWeight(UINT height) = 0;
    virtual cMtFont* SetHeight(UINT height) = 0;
    virtual cMtFont* Copy(cMtFont* cMtFont) = 0;
    virtual cMtFont* SetName(char* name) = 0;
    virtual cMtFont* SetText(char* text) = 0;
    virtual cMtFont* SetRect(RECT rect) = 0;
    virtual cMtFont* DrawShadow() = 0;
    virtual int GetTextHeight() = 0;
    virtual cMtFont* Release() = 0;
    virtual cMtFont* Load() = 0;
    virtual int Draw() = 0;
};
