#include <core.h>

#include "Classes/cMtPacketDump.h"
#include "Classes/cMtCustom.h"
#include "Classes/cMtLogs.h"

#include "Classes/cMtCreationCharacter.h"

void ASyncWaitForFramework()
{
  //Wait for the mtp.dll to be loaded
  while (!(pMtCore = GetFramework(LICENSE)))
    std::this_thread::sleep_for(100ms);

  //Setup OnLoad Callback
  pMtCore->OnLoadCallback([]()
  {
    //Console Logger
    #if defined(GLOBAL_USE_CONSOLE)
      cMtLogs::GetInstance()->Init();
    #endif

    //Packet Dumper
    #if (defined(GLOBAL_USE_PACKET_DUMP_CLIENT) || defined(GLOBAL_USE_PACKET_DUMP_SERVER))
      cMtPacketDump::GetInstance()->Init();
    #endif

    //Custom implementation template
    //cMtCreationCharacter::GetInstance()->Init();
    cMtCustom::GetInstance()->Init();
  });
}

void BeforeWaitForFramework()
{
  //Make sure to do the detours at this point
  //cMtCreationCharacter::SetupHooks();
  cMtCustom::DoTheDetours();
}

BOOL WINAPI DllMain(HMODULE hModule, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    DisableThreadLibraryCalls(hModule);

    BeforeWaitForFramework();
    std::thread(ASyncWaitForFramework).detach();
  }

  return TRUE;
}

extern "C" void __declspec(dllexport) Init() {};