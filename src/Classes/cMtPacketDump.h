#pragma once

#include <MtFramework/Utils/Abstract/Singleton.h>
#include <core.h>

#include "cMtLogs.h"

class cMtPacketDump : public Singleton<cMtPacketDump>
{
  private:
    template <typename T> bool Filter(T packet);
    template <typename T> void Dump(T packet);
    virtual void Encrypt(pC2SHeader packet);
    virtual void Decrypt(pS2CHeader packet);

  public:
    virtual cMtPacketDump* Init();
};