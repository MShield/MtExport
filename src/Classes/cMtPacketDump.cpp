#include "cMtPacketDump.h"

cMtPacketDump* cMtPacketDump::Init()
{
  #if defined (GLOBAL_USE_PACKET_DUMP_CLIENT)
    GetCore()->Cabal()->OnEncryptCallback(std::bind(&cMtPacketDump::Encrypt, this, _1));
  #endif

  #if defined (GLOBAL_USE_PACKET_DUMP_SERVER)
    GetCore()->Cabal()->OnDecryptCallback(std::bind(&cMtPacketDump::Decrypt, this, _1));
  #endif

  return this;
}

template <typename T> bool cMtPacketDump::Filter(T packet)
{
  UINT16 ignored_opcodes[] = { 0xC0, 0xBE, 0xC2, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xBF, 0x011F, 0x0187, 0x0188, 0x0191, 0x0193, 0x0195, 0x0196, 0x019C };
  bool result = true;

  result = (packet->length <= 62);

  for (auto i_opcode : ignored_opcodes)
  if (i_opcode == (typeid(T) == typeid(pC2SHeader) ? ((pC2SHeader)packet)->opcode : ((pS2CHeader)packet)->opcode))
  {
    result = false;
    break;
  }

  return result;
}

template <typename T> void cMtPacketDump::Dump(T packet)
{
  const char* kC2SFormat    = XorString("(C2S [%02X]) ");
  const char* kS2CFormat    = XorString("(S2C [%02X]) ");
  const char* kByteFormat   = XorString("%02X ");

#if defined(GLOBAL_USE_CONSOLE)
  if (this->Filter(packet))
  {
    int offset = (typeid(T) == typeid(pC2SHeader) ? offsetof(C2SHeader, opcode) : offsetof(S2CHeader, opcode));
    char buffer[20] = { 0 };
    std::string s_packet;

    sprintf_s(buffer, (typeid(T) == typeid(pC2SHeader) ? kC2SFormat : kS2CFormat), packet->length);
    s_packet = buffer;

    for (int i = offset; i < packet->length; i++)
    {
      sprintf_s(buffer, kByteFormat, *(UCHAR*)((UINT_PTR)packet + i));
      s_packet += buffer;
    }

    LOG(s_packet.c_str());
  }
#endif
}

void cMtPacketDump::Encrypt(pC2SHeader packet)
{
  Dump(packet);
}

void cMtPacketDump::Decrypt(pS2CHeader packet)
{
  Dump(packet);
}