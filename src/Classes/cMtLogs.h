#pragma once

#include <MtFramework/Utils/Abstract/Singleton.h>
#include <core.h>

#ifdef GLOBAL_USE_CONSOLE
  #define LOG(...) if (GetCore()) GetCore()->Utils()->DbgPrint(__VA_ARGS__)
#else
  #define LOG(...)
#endif

class cMtLogs : public Singleton<cMtLogs>
{
  public:
    virtual void Init();
};