#include "cMtCustom.h"
#include "cMtLogs.h"

cMtCustom* cMtCustom::Init()
{
  xPos = 200;
  yPos = 100;

  GetCore()->DirectX()->OnNotifyCreateDeviceCallback(std::bind(&cMtCustom::D3DCallBack_NotifyCreateDevice, this, _1, _2));
  GetCore()->DirectX()->OnAfterPresentCallback(std::bind(&cMtCustom::D3DCallBack_Present, this, _1));
  GetCore()->DirectX()->OnReleaseCallback(std::bind(&cMtCustom::D3DCallBack_Release, this, _1));

  GetCore()->Cabal()->OnBeforeAnnouncementCallback(std::bind(&cMtCustom::BeforeAnnouncementCallback, this, _1, _2, _3, _4, _5));
  GetCore()->Cabal()->OnPAPDMathCallback(std::bind(&cMtCustom::PAPDMathCallback, this, _1, _2));

  return this;
}

void cMtCustom::PAPDMathCallback(char* attack, char* defense)
{
  //Be aware that this procedure is called (1 * FPS/s), make sure not to call sprintf_s all the time
  int cSAttack = GetGame()->GetCharacterData(CharacterData::Attack);
  int cMAttack = GetGame()->GetCharacterData(CharacterData::MagicAttack);

  int cDefense = GetGame()->GetCharacterData(CharacterData::Defense);

  sprintf_s(attack,   kMaxPAPDStringSize, "%d", (cSAttack + cMAttack));
  sprintf_s(defense,  kMaxPAPDStringSize, "%d", cDefense);
}

bool cMtCustom::BeforeAnnouncementCallback(UINT32 itemID, UINT32 itemOpt, UINT32 characterID, UINT32 channel, UINT32 map)
{
  return true;
}

void cMtCustom::DoTheDetours()
{
  //To Do: Game's Detours
}

WMHandler(cMtCustom::OnClick)
{
  POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
  POINT pos = { instance.xPos, instance.yPos };

  RECT rect = _this->GetClientRect(pos);

  if (_this->IsInRect(pos, pt))
  {
    instance.xMPos = (pt.x - pos.x);
    instance.yMPos = (pt.y - pos.y);
    instance.canMove = true;

    *forceRet = true;
  }

  return 0;
}

WMHandler(cMtCustom::OnLButtonUp)
{
  if (instance.canMove)
    instance.canMove = false;

  return 0;
}

WMHandler(cMtCustom::OnRButtonUp)
{
  if (!(wParam & MK_LBUTTON))
    OnLButtonUp(hWnd, uMsg, wParam, lParam, _this, forceRet);

  return 0;
}

WMHandler(cMtCustom::OnMouseMove)
{
  POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
  POINT pos = { instance.xPos, instance.yPos };

  if (instance.canMove)
  {
    pt = instance.AdjustRect(hWnd, pt);

    instance.xPos = pt.x;
    instance.yPos = pt.y;
  }

  return 0;
}

WMHandler(cMtCustom::OnSize)
{
  if (wParam != SIZE_MINIMIZED)
  {
    POINT pt = { instance.xPos * 2, instance.yPos * 2 };

    instance.xMPos = instance.xPos;
    instance.yMPos = instance.yPos;

    pt = instance.AdjustRect(hWnd, pt);

    instance.xPos = pt.x;
    instance.yPos = pt.y;
  }

  return 0;
}

void cMtCustom::D3DCallBack_Release(Direct3DDevice9Wrapper* _this) { ReleaseTextures(); }

void cMtCustom::D3DCallBack_NotifyCreateDevice(Direct3D9Wrapper* pD3DWrapper, Direct3DDevice9Wrapper* pD3DDeviceWrapper)
{
  const char* kDataFolder = XorString("\\Data\\");

  wchar_t dir[256], dataFolder[40];

  asciiToUnicode((char*)kDataFolder, dataFolder, sizeof(dataFolder));
  GetModuleFileNameW(0, dir, sizeof(dir) / 2);
  lstrcatW(extractFileDirW(dir), dataFolder);

  pD3DDeviceWrapper->GetCreationParameters(&parameters);

  txBack = pMtCore->Create()->Texture();
  txUI2 = pMtCore->Create()->Texture();
  font = pMtCore->Create()->Font();

  txUI2->Create(pD3DDeviceWrapper, dir, (char*)XorString("UI\\Theme1\\UI_Texture2.dds"));
  font->Create(pD3DDeviceWrapper, 24, FW_MEDIUM, ANTIALIASED_QUALITY, (char*)XorString("arial"));

  textures.push_back(txBack->Copy(txUI2)->SetSize(109, 24)->SetRect(626, 234)->ScaleFromSize(260, 44)->SetPos(0, 0));

  txBack->SetupWMHandler(parameters.hFocusWindow)
        ->AddCallback(WM_LBUTTONDOWN, OnClick)
        ->AddCallback(WM_LBUTTONUP,   OnLButtonUp)
        ->AddCallback(WM_RBUTTONUP,   OnRButtonUp)
        ->AddCallback(WM_MOUSEMOVE,   OnMouseMove)
        ->AddCallback(WM_SIZE,        OnSize);
}

void cMtCustom::D3DCallBack_Present(Direct3DDevice9Wrapper* _this)
{
  cMtpvCabal* _cabal = GetGame();

  if ((_cabal) && (_cabal->IsCharLogged()))
  {
    D3DXVECTOR2 pos = D3DXVECTOR2((float)xPos, (float)yPos);
    POINT ptPos = { xPos, yPos };
    RECT rectText = txBack->GetClientRect(ptPos);

    Begin(_this, D3DXSPRITE_ALPHABLEND);
      DrawTexture(pos, txBack);
    End();

    font->SetRect(rectText)->SetTextAttr(0xFF396DCE, DT_CENTER | DT_VCENTER)->SetText((char*)XorString("MShield Protect D3D UI"))->DrawShadow();
  }
}