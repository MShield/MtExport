#pragma once

#include <MtFramework/Utils/Abstract/Singleton.h>
#include <MtFramework/Utils/Abstract/cMtaD3DUI.h>
#include <MtFramework/DirectX/Direct3D9Wrapper.h>
#include <MtFramework/cMtpvCore.h>
#include <core.h>

#include <WindowsX.h>
#include <d3dx9.h>

class cMtCustom : cMtaD3DUI, public Singleton<cMtCustom>
{
  private:
    const int kMaxPAPDStringSize = 256;

    cMtTexture* txBack = 0;
    cMtTexture* txUI2 = 0;
    bool downClosed = false;
    bool canMove = false;

    virtual POINT AdjustRect(HWND hWnd, POINT pt) { return cMtaD3DUI::AdjustRect(hWnd, pt, this->txBack->GetWidth(), this->txBack->GetHeight()); };

    virtual bool BeforeAnnouncementCallback(UINT32 itemID, UINT32 itemOpt, UINT32 characterID, UINT32 channel, UINT32 map);
    virtual void PAPDMathCallback(char* attack, char* defense);

    virtual void D3DCallBack_NotifyCreateDevice(Direct3D9Wrapper* pD3DWrapper, Direct3DDevice9Wrapper* pD3DDeviceWrapper);
    virtual void D3DCallBack_Present(Direct3DDevice9Wrapper* _this);
    virtual void D3DCallBack_Release(Direct3DDevice9Wrapper* _this);

    static WMHandler(OnLButtonUp);
    static WMHandler(OnRButtonUp);
    static WMHandler(OnMouseMove);
    static WMHandler(OnClick);
    static WMHandler(OnSize);

  public:
    static void DoTheDetours();

    virtual cMtCustom* Init();
};