#pragma once

#include <Windows.h>

void* basicHook(void* target, void* newFunction, int size = 5, UCHAR hkbyte = 0xE9);
UINT32 asciiToUnicode(char* aStr, wchar_t* uStr, UINT32 uSize);
UINT32 unicodeToAscii(wchar_t* uStr, char* aStr, UINT32 aSize);
int strposW(wchar_t* haystack, wchar_t* needle);
int strpos(char* haystack, char* needle);
wchar_t* extractFileDirW(wchar_t* s);
char* extractFileDir(char* s);