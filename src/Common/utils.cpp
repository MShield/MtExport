#include "utils.h"

UINT32 asciiToUnicode(char* aStr, wchar_t* uStr, UINT32 uSize)
{
  UINT32 i = 0;

  while ((i <= (uSize - 1)) && (*(UINT8*)((UINT64)aStr + i)))
  {
    *(UINT16*)((UINT64)uStr + ((UINT64)i * 2)) = (UINT16)(*(UINT8*)((UINT64)aStr + i));
    i++;
  }
  *(UINT16*)((UINT64)uStr + (UINT64)i * 2) = 0;

  return i;
}

UINT32 unicodeToAscii(wchar_t* uStr, char* aStr, UINT32 aSize)
{
  UINT32 i = 0;

  while ((i <= (aSize - 1)) && (*(short*)((UINT64)uStr + ((UINT64)i << 1))))
  {
    *(UINT8*)((UINT64)aStr + i) = *(UINT8*)((UINT64)(uStr)+((UINT64)i << 1));
    i++;
  }
  *(UINT8*)((UINT64)aStr + i) = 0;

  return i;
}

int strpos(char* haystack, char* needle)
{
  char* p = strstr(haystack, needle);

  if (p)
    return (int)(p - haystack);
  return -1;
}

int strposW(wchar_t* haystack, wchar_t* needle)
{
  wchar_t* p = wcsstr(haystack, needle);

  if (p)
    return (int)(p - haystack);
  return -1;
}

char* extractFileDir(char* s)
{
  if (s && (strpos(s, (char*)"\\") >= 0))
  {
    int i = lstrlenA(s);
    while ((s[i] != '\\') && i)
      s[i--] = 0;
    if (i)
      s[i] = 0;
  }

  return s;
}

wchar_t* extractFileDirW(wchar_t* s)
{
  if (s && (strposW(s, (wchar_t*)L"\\") >= 0))
  {
    int i = lstrlenW(s);
    while ((s[i] != '\\') && i)
      s[i--] = 0;
    if (i)
      s[i] = 0;
  }

  return s;
}

void* basicHook(void* target, void* newFunction, int size, UCHAR hkbyte)
{
  DWORD oldProtect;
  void* oldFunc;

  oldFunc = (void*)VirtualAllocEx(INVALID_HANDLE_VALUE, 0, 10, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

  if (oldFunc)
  {
    if (VirtualProtectEx(INVALID_HANDLE_VALUE, target, size, PAGE_EXECUTE_READWRITE, &oldProtect))
    {
      *(UCHAR*)oldFunc = *(UCHAR*)target;
      *(UCHAR*)((UINT_PTR)oldFunc + 5) = 0xE9;
      *(UINT*)((UINT_PTR)oldFunc + 1) = *(UINT*)((UINT_PTR)target + 1);
      *(UINT*)((UINT_PTR)oldFunc + 6) = (UINT)((UINT_PTR)target - (UINT_PTR)oldFunc - 5);

      FillMemory(target, size, 0x90);
      *(UCHAR*)target = hkbyte;
      *(UINT*)((UINT_PTR)target + 1) = (UINT)((UINT_PTR)newFunction - (UINT_PTR)target - 5);
      VirtualProtectEx(INVALID_HANDLE_VALUE, target, size, oldProtect, &oldProtect);
    }
  }

  return oldFunc;
}