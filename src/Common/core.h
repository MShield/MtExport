#pragma once

#include "config.h"
#include <Windows.h>
#include <chrono>
#include <thread>

#include <MtFramework/DirectX/DirectXDependences.h>
#include <MtFramework/cMtpvCabal.h>
#include <MtFramework/cMtpvCore.h>
#include <utils.h>

extern inline cMtpvCore* pMtCore = 0;

cMtpvCabal* GetGame();
cMtpvCore* GetCore();

using namespace std::chrono_literals;