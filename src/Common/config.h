#pragma once

#include "../Addons/XorString.h"

//########################################
//  License
//########################################
#define LICENSE XorString("Internal license goes here")


//########################################
//  Global Configs
//########################################
#define GLOBAL_USE_CONSOLE
//#define GLOBAL_USE_PACKET_DUMP_CLIENT
#define GLOBAL_USE_PACKET_DUMP_SERVER